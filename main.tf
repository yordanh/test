resource "random_id" "server" {
  keepers = {}

  byte_length = 10
}

resource "null_resource" "server" {
  provisioner "local-exec" {
    command = "echo ${random_id.server.hex}"
  }
}


